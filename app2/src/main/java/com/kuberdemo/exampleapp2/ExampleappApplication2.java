package com.kuberdemo.exampleapp2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExampleappApplication2 {

	public static void main(String[] args) {
		SpringApplication.run(ExampleappApplication2.class, args);
	}

}
