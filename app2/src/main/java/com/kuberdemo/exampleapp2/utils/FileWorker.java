package com.kuberdemo.exampleapp2.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.*;

@Component
public class FileWorker {

    @Value("${exampleapp.filepath}")
    private String filePath;

    private final String FILE_NAME = "testfile";

    public void write() {
        String now = read();
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath + FILE_NAME))) {
            writer.write(now);
            writer.write("test");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String read() {
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath+FILE_NAME))) {
            StringBuilder out = new StringBuilder();
            while (reader.ready()) {
                out.append(reader.readLine());
            }
            return out.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
}
