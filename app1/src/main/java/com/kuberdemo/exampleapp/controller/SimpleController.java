package com.kuberdemo.exampleapp.controller;

import com.kuberdemo.exampleapp.utils.FileWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Date;

@RestController
@RequestMapping("/app1")
@RequiredArgsConstructor
public class SimpleController {

    private final FileWorker fileWorker;

    @Value("${exampleapp.externalapp}")
    private String serverIp;

    @Value("${exampleapp.ip}")
    private String myIp;


    @GetMapping("/test1")
    public String getInfo() {
        StringBuilder builder = new StringBuilder();
        builder.append("response from exampleapp \n");
        builder.append(new Date());
        return builder.toString();
    }

    @GetMapping("/app1const")
    public String app1const() {
        System.out.println(serverIp);
        RestTemplate restTemplate = new RestTemplate();
        return serverIp + "\n" +
                restTemplate.getForObject(serverIp, String.class);
    }

    @GetMapping("/myip")
    public String myIp() {
        return myIp;
    }

    @GetMapping("/write")
    public String write() {
        fileWorker.write();
        return "done";
    }

    @GetMapping("/read")
    public String read() {
        return fileWorker.read();
    }
}
